import { message as messages, user as users } from '../data/db';
import json from '../data/neighborhoods.json';
import { removeSpecialChars } from './mask';
import { Message, Neighborhood, Neighborhoods, User } from './types';

export const groupItems = (arr: Array<any>, groupBy: string) => arr.reduce((result: any, item: any) => {
  // eslint-disable-next-line no-param-reassign
  (result[item[groupBy]] = result[item[groupBy]] || []).push(item);
  return result;
}, {});

export const searchNeighborhoodsByName = (searchTerm: string) => json.neighborhoods.reduce(
  (resultCategory: Array<Neighborhoods>, neighborhoods: Neighborhoods) => {
    const resultSubcategory: Array<Neighborhood> = neighborhoods.neighborhood.reduce(
      (resultSubcategory: Array<Neighborhood>, neighborhood: Neighborhood) => {
        if (removeSpecialChars(neighborhood.name).includes(removeSpecialChars(searchTerm)))
          // && searchTerm !== "")
          resultSubcategory.push(neighborhood);

        return resultSubcategory;
      }, [])
    if (resultSubcategory.length > 0)
      resultCategory.push({ zone: neighborhoods.zone, neighborhood: resultSubcategory });

    return resultCategory;
  }, [])

export const searchMessagesByNeighborhood = (zone: number, idNeighborhood: number) => {
  return messages.reduce((result: Message[], message: Message) => {
    if (message.neighborhoodZone.zone === zone
      && message.neighborhoodZone.neighborhood.id === idNeighborhood)
      result.push(message)
    return result;
  }, [])
};

export const searchMessagesByUser = (idUser: number) => {
  return messages.reduce((result: Message[], message: Message) => {
    if (message.user.id === idUser)
      result.push(message)
    return result;
  }, [])
};

export const getUserById = (id: number): User | undefined => {
  let user = undefined;
  users.forEach(aux => { if (aux.id === id) user = aux });
  return user;
}

export const includeMessage = (text: string, user: User, zone: number, idNeighborhood: number) => {
  let aux = undefined;
  json.neighborhoods.forEach(((neighborhoods) => {
    neighborhoods.neighborhood.forEach((neighborhood) => {
      if (neighborhood.id === idNeighborhood)
        aux = neighborhood;
    })
  }))
  aux && messages.push({ text, user, neighborhoodZone: { zone, neighborhood: aux } });
}