import { environment } from '../environment/environment';
import { UserAuth } from "./types";
import { user as users } from '../data/db';

const {
  REACT_APP_LOCAL_STORAGE_USER_AUTH,
} = environment;

const userAuth = {
  token: localStorage.getItem(REACT_APP_LOCAL_STORAGE_USER_AUTH) || '""',
};

export const isAuthenticated = () => {
  const token = JSON.parse(userAuth.token);
  if (token !== '') {
    return true;
  }
  return false;
};


export const getToken = () => {
  const token = JSON.parse(userAuth.token);
  if (token !== '') {
    return token;
  }
  return '';
};

export const authenticateUser = (userAuth: UserAuth) => {
  let user;

  users.forEach(aux => {
    if (aux.userAuth.username === userAuth.username
      && aux.userAuth.password === userAuth.password) {
      user = {
        id: aux.id,
        name: aux.name,
        userAuth: {
          token: aux.userAuth.token
        }
      };
    }
  })

  return user;

}

export const validateToken = (token: String) => {
  let user;

  users.forEach(aux => {
    if (aux.userAuth.token === token) {
      user = aux;
    }
  })

  return user;
}