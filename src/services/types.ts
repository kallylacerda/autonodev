export interface User {
  id: number;
  name: string;
  userAuth: UserAuth;
}

export interface UserAuth {
  username: string;
  password: string;
  token?: string;
}

export interface Neighborhoods {
  zone: number;
  neighborhood: Neighborhood[];
}

export interface Neighborhood {
  id: number;
  name: string;
}

export interface NeighborhoodWithZone {
  zone: number;
  neighborhood: Neighborhood;
}

export interface Message {
  text: string;
  user: User;
  neighborhoodZone: NeighborhoodWithZone;
}

export interface MessagesByNeighborhood {
  text: string[];
  neighborhood: Neighborhood;
}

export interface MessagesByUser {
  text: string[];
  user: User;
}