import React, { ReactNode } from "react";
import { Neighborhoods } from "../../services/types";

import Category from "../Category";

import "./styles.scss";

type Props = {
  data: Neighborhoods[];
  children?: ReactNode;
};

const CategoryList: React.FC<Props> = (props: Props) => {
  const data: Neighborhoods[] = props.data;

  return (
    <div className="category-list">
      <ul>
        {data.map((category, index) => (
          <li key={index}>
            <Category data={category} />
          </li>
        ))}
      </ul>
    </div>
  );
};

export default CategoryList;
