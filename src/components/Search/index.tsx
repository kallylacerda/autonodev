import React, { useState } from "react";

import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";

import JPcoatArms from "../../images/brasao_jp.svg";

import { searchNeighborhoodsByName } from "../../services/functions";
import { Neighborhoods } from "../../services/types";

import DisplaySearch from "../DisplaySearch";

import "./styles.scss";

const Search: React.FC = () => {
  const [term, setTerm] = useState<string>("");
  const [searchedNeighborhood, setSearchedNeighborhood] = useState<
    Neighborhoods[]
  >();

  // FUNCTIONS

  function handleSetSearchedNeighborhood(
    term: string
  ): void {
    setSearchedNeighborhood(searchNeighborhoodsByName(term));
  }

  function handleClick(ce: React.MouseEvent<HTMLButtonElement>): void {
    ce.preventDefault();
    handleSetSearchedNeighborhood(term);
  }

  function handleKeyPressed(ce: React.KeyboardEvent<HTMLInputElement>): void {
    if (ce.key === "Enter") {
      ce.preventDefault();
      handleSetSearchedNeighborhood(term);
    }
  }

  async function handleText(
    ce: React.ChangeEvent<HTMLInputElement>
  ): Promise<void> {
    await setTerm(ce.target.value);
  }

  // TEMPLATE

  return (
    <div className="search">
      <img className="w-25" src={JPcoatArms} alt="João Pessoa coat of arms" />

      <Form>
        <Form.Label className="mt-3">Type your search</Form.Label>
        <Form.Control
          size="lg"
          value={term}
          onChange={handleText}
          onKeyPress={handleKeyPressed}
        ></Form.Control>
        <Button
          className="mt-2 float-right"
          variant="light"
          onClick={handleClick}
        >
          Search
        </Button>
      </Form>

      {searchedNeighborhood ===
      undefined ? null : searchedNeighborhood.length === 0 ? (
        <span>Nothing found.</span>
      ) : (
        <DisplaySearch data={searchedNeighborhood}></DisplaySearch>
      )}
    </div>
  );
};

export default Search;
