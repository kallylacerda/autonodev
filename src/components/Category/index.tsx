import React, { ReactNode, useState } from "react";
import { Neighborhoods } from "../../services/types";

import Subcategory from "../Subcategory";

import "./styles.scss";

type Props = {
  data: Neighborhoods;
  children?: ReactNode;
};

const Category: React.FC<Props> = (props: Props) => {
  const [viewSubcategory, setViewSubcategory] = useState(false);

  const neighborhood: Neighborhoods = props.data;

  return (
    <>
      <span
        className="category"
        onClick={() => setViewSubcategory(!viewSubcategory)}
      >
        Zone {neighborhood.zone}
      </span>
      {viewSubcategory && (
        <Subcategory data={neighborhood.neighborhood} zone={neighborhood.zone}/>
      )}
    </>
  );
};

export default Category;
