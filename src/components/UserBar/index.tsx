import React, { useState } from "react";
import Button from "react-bootstrap/esm/Button";
import { useDispatch, useSelector } from "react-redux";
import { Link, useHistory } from "react-router-dom";
import { ApplicationState } from "../../store";
import { removeUser } from "../../store/ducks/user/actions";
import Auth from "../Auth";
import "./styles.scss";

const UserC: React.FC = () => {
  const [showModal, setShowModal] = useState(false);
  const userSelector = useSelector(({ user }: ApplicationState) => user.user);

  const history = useHistory();

  const dispatch = useDispatch();

  const logout = () => {
    dispatch(userSelector && removeUser());
    history.push('/');
  }

  return (
    <div className="user">
      {userSelector !== undefined &&
      userSelector.id !== 0 &&
      userSelector.name !== "" ? (
        <>
          <Link to="/user" className="user-name" >{userSelector.name}</Link>

          <Button variant="secondary" size="sm" onClick={logout}>
            Logout
          </Button>
        </>
      ) : (
        <Button variant="danger" onClick={() => setShowModal(true)}>
          Login
        </Button>
      )}
      {showModal && (
        <Auth showModal={showModal} closeModal={() => setShowModal(false)} />
      )}
    </div>
  );
};

export default UserC;
