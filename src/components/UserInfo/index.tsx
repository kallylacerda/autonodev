import React, { useEffect, useState } from "react";
import { Col, Form, InputGroup } from "react-bootstrap";
import { useSelector } from "react-redux";
import { getUserById } from "../../services/functions";
import { User } from "../../services/types";
import { ApplicationState } from "../../store";
import "./styles.scss";

const UserInfo: React.FC = () => {
  const [user, setUser] = useState<User>();
  const userSelector = useSelector(({ user }: ApplicationState) => user.user);

  useEffect(() => {
    if (!user) setUser(getUserById(userSelector.id));
  }, [user, userSelector]);

  return (
    <div className="user-info">
      <Col xs={4}>
        <InputGroup className="mb-2">
          <InputGroup.Prepend>
            <InputGroup.Text>#</InputGroup.Text>
          </InputGroup.Prepend>
          <Form.Control readOnly defaultValue={user?.id} />
        </InputGroup>
        <InputGroup className="mb-2">
          <InputGroup.Prepend>
            <InputGroup.Text>@</InputGroup.Text>
          </InputGroup.Prepend>
          <Form.Control readOnly defaultValue={user?.userAuth.username} />
        </InputGroup>
        <InputGroup>
          <InputGroup.Prepend>
            <InputGroup.Text>Name</InputGroup.Text>
          </InputGroup.Prepend>
          <Form.Control readOnly defaultValue={user?.name} />
        </InputGroup>
      </Col>
    </div>
  );
};

export default UserInfo;
