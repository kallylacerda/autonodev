import React, { ReactNode, useEffect, useState } from "react";
import { Modal } from "react-bootstrap";
import {
  searchMessagesByNeighborhood,
  searchMessagesByUser,
} from "../../services/functions";
import { Message } from "../../services/types";
import TypeMessage from "../TypeMessage";
import "./styles.scss";

type Props = {
  showModal: boolean;
  closeModal: () => void;
  type: "user" | "neighborhood";
  zone: number;
  neighborhood?: string;
  id: number;
  children?: ReactNode;
};

const MessageModal: React.FC<Props> = (props: Props) => {
  const [messages, setMessages] = useState<Message[]>();
  const [hasNewMessage, setHasNewMessage] = useState<boolean>(true);

  useEffect(() => {
    if (
      props.type === "neighborhood" &&
      props.zone !== undefined &&
      hasNewMessage
    ) {
      setMessages(searchMessagesByNeighborhood(props.zone, props.id));
      setHasNewMessage(false);
    } else if (props.type === "user" && hasNewMessage) {
      setMessages(searchMessagesByUser(props.id));
      setHasNewMessage(false);
    }
  }, [props.type, props.zone, props.id, hasNewMessage]);

  return (
    <div className="message">
      <Modal show={props.showModal} size="lg" onHide={props.closeModal}>
        <Modal.Header closeButton>
          {props.neighborhood !== undefined ? (
            <Modal.Title>
              Zone: {props.zone} <br />
              Neighborhood <br/>
              Id: {props.id} - Name: {props.neighborhood}
            </Modal.Title>
          ) : (
            <Modal.Title>Messages</Modal.Title>
          )}
        </Modal.Header>
        <Modal.Body className="modal-body">
          {messages === undefined ? null : messages.length === 0 ? (
            <span>Nothing found.</span>
          ) : (
            messages.map((message, index) => (
              <span key={index}>
                {message.user.name} says: {message.text}
                <br />
              </span>
            ))
          )}
          <TypeMessage
            zone={props.zone}
            idNeighborhood={props.id}
            hasNewMessage={() => setHasNewMessage(true)}
          />
        </Modal.Body>
      </Modal>
    </div>
  );
};

export default MessageModal;
