import React, { ReactNode, useEffect, useState, useCallback } from "react";
import { Modal } from "react-bootstrap";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import { useDispatch } from "react-redux";
import flag from "../../images/bandeira_jp.svg";
import { authenticateUser } from "../../services/auth";
import { User, UserAuth } from "../../services/types";
import { updateUser } from "../../store/ducks/user/actions";
import "./styles.scss";

type Props = {
  showModal: boolean;
  closeModal: () => void;
  children?: ReactNode;
  text?: string;
};

const Auth: React.FC<Props> = (props: Props) => {
  const [userAuth, setUserAuth] = useState<UserAuth>({
    username: "",
    password: "",
  });
  const [user, setUser] = useState<User>();
  const [showModalResp, setShowModalResp] = useState(false);

  const dispatch = useDispatch();

  const dispatchSetUser = useCallback(
    (user: User) => {
      dispatch(
        user.userAuth.token &&
          updateUser({
            user: { id: user.id, name: user.name, token: user.userAuth.token },
          })
      );
    },
    [dispatch]
  );

  useEffect(() => {
    if (user) dispatchSetUser(user);
  }, [dispatchSetUser, user]);

  const handleUsername = (ce: React.ChangeEvent<HTMLInputElement>) => {
    setUserAuth({ ...userAuth, username: ce.currentTarget.value });
  };

  const handlePassword = (ce: React.ChangeEvent<HTMLInputElement>) => {
    setUserAuth({ ...userAuth, password: ce.currentTarget.value });
  };

  const handleClick = () => {
    setUser(() => authenticateUser(userAuth));
    setShowModalResp(true);
  };

  const handleKeyPressed = (ce: React.KeyboardEvent<HTMLInputElement>) => {
    if (ce.key === "Enter") {
      setUser(() => authenticateUser(userAuth));
      setShowModalResp(true);
    }
  };

  return (
    <>
      <Modal show={props.showModal} onHide={props.closeModal}>
        <Modal.Header closeButton>
          <Modal.Title>Login</Modal.Title>
        </Modal.Header>
        <Modal.Body className="modal-body">
          <span className="modal-title">AutonoDev Test</span>
          {props.text ? (
            <h4 className="ml-5">{props.text}</h4>
          ) : (
            <img src={flag} alt="Flag" width="50%" />
          )}
          <Form>
            <Form.Control
              className="mt-4"
              size="lg"
              placeholder="Username"
              value={userAuth.username}
              onChange={handleUsername}
              onKeyPress={handleKeyPressed}
            ></Form.Control>
            <Form.Control
              className="mt-3"
              size="lg"
              type="password"
              placeholder="Password"
              value={userAuth.password}
              onChange={handlePassword}
              onKeyPress={handleKeyPressed}
            ></Form.Control>
            <Button
              variant="danger"
              className="mt-4 mb-5 float-right"
              onClick={handleClick}
            >
              Enter
            </Button>
          </Form>
        </Modal.Body>
      </Modal>

      <Modal
        show={showModalResp}
        centered
        onHide={() => {
          user ? props.closeModal() : setShowModalResp(false);
        }}
      >
        <Modal.Body className="text-center">
          {user ? "Success" : "Username or password error"}
        </Modal.Body>
      </Modal>
    </>
  );
};

export default Auth;
