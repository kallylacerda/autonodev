import React from "react";

import Categories from "../../../containers/Categories";

import "./styles.scss";

const Nav: React.FC = () => {
  return (
    <div className="nav">
      <Categories />
    </div>
  );
};

export default Nav;
