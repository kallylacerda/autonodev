import React from "react";
import { Link } from "react-router-dom";

import LoggedUser from "../../../containers/LoggedUser";

import "./styles.scss";

const Header: React.FC = () => {
  return (
    <div className="header">
      <div className="title">
        <Link to="/">
          <span>AutonoDev</span>
        </Link>
      </div>
      <LoggedUser />
    </div>
  );
};

export default Header;
