import React, { ReactNode } from "react";

import "./styles.scss";

type Props = {
  children?: ReactNode;
};

const Content: React.FC<Props> = (props: Props) => {
  return (
    <main className="content">
      {props.children}
    </main>
  );
};

export default Content;