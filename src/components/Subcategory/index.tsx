import React, { ReactNode, useState } from "react";
import { Neighborhood } from "../../services/types";
import MessageModal from "../MessageModal";

import "./styles.scss";

type Props = {
  data: Neighborhood[];
  zone: number;
  children?: ReactNode;
};

const Subcategory: React.FC<Props> = (props: Props) => {
  const [showModal, setShowModal] = useState<string>();

  const data: Neighborhood[] = props.data;

  const getModal = (value: string) => {
    setShowModal(value);
  };

  const hideModal = () => {
    setShowModal(undefined);
  };

  return (
    <>
      <ul>
        {data.map((item, index) => {
          return (
            <li key={index}>
              <div className="subcategory" onClick={() => getModal(item.name)}>
                {item.name}
              </div>

              {showModal === item.name && (
                <MessageModal
                  showModal={showModal === item.name}
                  closeModal={() => hideModal()}
                  type="neighborhood"
                  zone={props.zone}
                  neighborhood={item.name}
                  id={item.id}
                />
              )}
            </li>
          );
        })}
      </ul>
    </>
  );
};

export default Subcategory;
