import React, { ReactNode, useState } from "react";

import { Neighborhoods } from "../../services/types";
import MessageModal from "../MessageModal";

import "./styles.scss";

type Props = {
  data: Neighborhoods[];
  children?: ReactNode;
};

const DisplaySearch: React.FC<Props> = (props: Props) => {
  const [showModal, setShowModal] = useState<string>();

  const getModal = (value: string) => {
    setShowModal(value);
  };

  const hideModal = () => {
    setShowModal(undefined);
  };

  return (
    <div style={{ width: "20%" }}>
      <ul>
        {props.data?.map((neighborhoods, index) => {
          return (
            <li key={index}>
              <section className="zone">Zone {neighborhoods.zone}</section>

              {neighborhoods.neighborhood.map((neighborhood, index) => {
                return (
                  <ul key={index}>
                    <li key={index}>
                      <span
                        className="modal-neighborhood"
                        onClick={() => getModal(neighborhood.name)}
                      >
                        {neighborhood.name}
                      </span>
                      {showModal === neighborhood.name && (
                        <MessageModal
                          showModal={showModal === neighborhood.name}
                          closeModal={() => hideModal()}
                          type="neighborhood"
                          zone={neighborhoods.zone}
                          neighborhood={neighborhood.name}
                          id={neighborhood.id}
                        />
                      )}
                    </li>
                  </ul>
                );
              })}
            </li>
          );
        })}
      </ul>
    </div>
  );
};

export default DisplaySearch;
