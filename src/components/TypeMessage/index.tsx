import React, { useState } from "react";
import { InputGroup } from "react-bootstrap";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import { useSelector } from "react-redux";
import { getUserById, includeMessage } from "../../services/functions";
import { ApplicationState } from "../../store";
import Auth from "../Auth";
import "./styles.scss";

type Props = {
  zone: number;
  idNeighborhood: number;
  hasNewMessage(): void;
};

const TypeMessage: React.FC<Props> = (props: Props) => {
  const [text, setText] = useState<string>("");
  const [showModalLogin, setShowModalLogin] = useState<boolean>();
  const user = getUserById(
    useSelector(({ user }: ApplicationState) => user.user).id
  );

  const handleClick = (ce: React.MouseEvent<HTMLButtonElement>) => {
    sendMessage();
  };

  const handleKeyPressed = (ce: React.KeyboardEvent<HTMLTextAreaElement>) => {
    if (ce.key === "Enter" && !ce.shiftKey) {
      ce.preventDefault();
      sendMessage();
    }
  };

  const sendMessage = () => {
    if (user) {
      includeMessage(text, user, props.zone, props.idNeighborhood);
      setText("");
      props.hasNewMessage();
    } else setShowModalLogin(true);
  };

  const handleText = (ce: React.ChangeEvent<HTMLTextAreaElement>) => {
    setText(ce.target.value);
  };

  return (
    <div className="type-message">
      <InputGroup className=" mt-4">
        <InputGroup.Prepend>
          <InputGroup.Text>Type your message</InputGroup.Text>
          <Form.Control
            size="lg"
            as="textarea"
            rows={3}
            value={text}
            onChange={handleText}
            onKeyPress={handleKeyPressed}
          />
        </InputGroup.Prepend>
      </InputGroup>
      <Button
        className="mt-2 float-right"
        variant="light"
        onClick={handleClick}
      >
        Send
      </Button>

      {showModalLogin && (
        <Auth
          text="User isn't logged in. Only logged users can send messages, please loggin."
          showModal={showModalLogin}
          closeModal={() => setShowModalLogin(false)}
        />
      )}
    </div>
  );
};

export default TypeMessage;
