import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import SearchSearch from '../../containers/SearchSearch';
import InfoUser from '../../containers/InfoUser';
import PrivateRoute from '../_PrivateRoute';

export default function Routes() {
  return (
    <Switch>
      <Route exact path="/" component={SearchSearch} />
      <PrivateRoute exact path="/user" component={InfoUser} />
      <Redirect from="*" to="/" />
    </Switch>
  );
}