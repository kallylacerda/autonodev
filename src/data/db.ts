import { Message, User } from "../services/types";
import json from "./neighborhoods.json";


export const user: User[] =
    [
        {
            id: 1,
            name: "José da Silva",
            userAuth: {
                username: "usuario",
                password: "123456",
                token: "tokenjose",
            }
        },
        {
            id: 2,
            name: "Manoel Souza",
            userAuth: {
                username: "man",
                password: "123",
                token: "tokenmanoel",
            }
        }
    ]

export const message: Message[] =
    [
        {
            text: "O bairro é muito legal",
            user: user[0],
            neighborhoodZone: {
                zone: json.neighborhoods[1].zone,
                neighborhood: json.neighborhoods[1].neighborhood[1]
            }
        },
        {
            text: "Muito limpo",
            user: user[0],
            neighborhoodZone: {
                zone: json.neighborhoods[2].zone,
                neighborhood: json.neighborhoods[2].neighborhood[1]
            }
        },
        {
            text: "As praças estão sempre lotadas!",
            user: user[0],
            neighborhoodZone: {
                zone: json.neighborhoods[2].zone,
                neighborhood: json.neighborhoods[2].neighborhood[1]
            }
        }
    ]