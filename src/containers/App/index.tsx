import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
import { BrowserRouter } from "react-router-dom";
import Content from "../../components/Template/Content";
import Header from "../../components/Template/Header";
import Nav from "../../components/Template/Nav";
import { environment } from "../../environment/environment";
import Routes from "../../routes/__Routes";
import { getToken, validateToken } from "../../services/auth";
import { User } from "../../services/types";
import { updateUser } from "../../store/ducks/user/actions";
import "./styles.scss";

const App = () => {
  const dispatch = useDispatch();

  useEffect(() => verifyToken());

  const dispatchSetUser = (user: User) => {
    dispatch(
      user.userAuth.token &&
        updateUser({
          user: { id: user.id, name: user.name, token: user.userAuth.token },
        })
    );
  };

  const verifyToken = () => {
    const token = getToken();

    if (token) {
      const user = validateToken(token);
      if (user) {
        dispatchSetUser(user);
      } else {
        localStorage.removeItem(environment.REACT_APP_LOCAL_STORAGE_USER_AUTH);
      }
    }
  };

  return (
    <div className="app">
      <BrowserRouter>
        <Header />
        <Nav />
        <Content>
          <Routes />
        </Content>
      </BrowserRouter>
    </div>
  );
};

export default App;
