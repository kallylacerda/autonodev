import React from "react";
import CategoryList from "../../components/CategoryList";
import { Neighborhoods } from "../../services/types";

import jsonFile from "../../data/neighborhoods.json";

const Categories: React.FC = () => {
  const data: Neighborhoods[] = jsonFile.neighborhoods;

  return <CategoryList data={data} />;
};

export default Categories;
