/**
 * Action types
 * @UPDATE_USER update user infos
 * @REMOVE_USER remove user infos
 */
export enum UserActionTypes {
  UPDATE_USER = '@user/UPDATE_USER',
  REMOVE_USER = '@user/REMOVE_USER'
}

/**
 * Data types
 * @name : name of user
 * @token : token of user
 */

export interface User {
  id: number
  name: string
  token: string
 }

/**
 * State type
 * @data : the user
 */
export interface UserState {
  readonly user: User
}
