import { environment } from './../../../environment/environment';
import { Reducer } from 'redux';
import { UserState, UserActionTypes } from './types';

const INITIAL_STATE: UserState = {
  user: {
    id: NaN,
    name: '',
    token: ''
  },
};

const reducer: Reducer<UserState> = (
  state = INITIAL_STATE,
  action,
) => {

  switch (action.type) {
    case UserActionTypes.UPDATE_USER:
      const updatedUserState = action.payload.userState;
      localStorage.setItem(
        environment.REACT_APP_LOCAL_STORAGE_USER_AUTH,
        JSON.stringify(updatedUserState.user.token),
      );
      return { ...state, ...updatedUserState };

    case UserActionTypes.REMOVE_USER:
      localStorage.removeItem(
        environment.REACT_APP_LOCAL_STORAGE_USER_AUTH,
      );
      return { ...state, ...INITIAL_STATE };

    default:
      return state;
  }
};

export default reducer;
