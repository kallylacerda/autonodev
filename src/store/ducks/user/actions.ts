import { action } from 'typesafe-actions';
import {
  UserActionTypes, UserState,
} from './types';

export const updateUser = (userState: UserState) =>
  action(UserActionTypes.UPDATE_USER, { userState })

export const removeUser = () =>
  action(UserActionTypes.REMOVE_USER)
